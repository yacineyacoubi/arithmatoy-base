#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "utils.h"

int VERBOSE = 0;

const char *get_all_digits() { return "0123456789abcdefghijklmnopqrstuvwxyz"; }
const size_t ALL_DIGIT_COUNT = 36;

void arithmatoy_free(char *number) { free(number); }

char *arithmatoy_add(unsigned int base, const char *lhs, const char *rhs) {
  if (VERBOSE) {
    fprintf(stderr, "add: entering function\n");
  }

  // Fill the function, the goal is to compute lhs + rhs
  // You should allocate a new char* large enough to store the result as a
  // string Implement the algorithm Return the result
   // reamoving leading zeros from `lhs` and `rhs`
  lhs = drop_leading_zeros(lhs);
  rhs = drop_leading_zeros(rhs);

  // calculate the length og the strings `lhs` and `rhs`
  size_t length_lhs = strlen(lhs), length_rhs = strlen(rhs);

  // Initialize variables for addition
  size_t addition, digit;
  unsigned int carry = 0;
  size_t maximum_length = (length_lhs > length_rhs) ? length_lhs : length_rhs;
  size_t result_i = maximum_length + 1;
  
  // Memo allocation to save result, one byte for NULL character and another one for the carry
  char *result = calloc(maximum_length + 2, sizeof(char));
  if (!result){
   debug_abort("Memory allocation failed "); 
  }

  // going through `lhs` and `rhs` from the right side to the left side, addinig digit by digit and saving the result -> `result`
  while (length_lhs > 0 || length_rhs > 0 || carry != 0) {
    // getting the digits values to add
    size_t digit_lhs = (length_lhs > 0 ? get_digit_value(lhs[--length_lhs]) : 0);
    size_t digit_rhs = (length_rhs > 0 ? get_digit_value(rhs[--length_rhs]) : 0);

    // if verbose activated, print the result
    if (VERBOSE) {
      fprintf(stderr, "add: Digit %c Digit %c carry %u\n", to_digit(digit_lhs), to_digit(digit_rhs), carry);
    }

    // calculate the addition and the carry
    addition = digit_lhs + digit_rhs + carry;
    carry = addition / base;
    digit = addition % base;

    // if verbose activated, print the result
    if (VERBOSE) {
      fprintf(stderr, "add: result: Digit %c carry %u\n", to_digit(digit), carry);
    }

    // save the result -> `result`
    result[--result_i] = to_digit(digit);
  }

  // if verbose activated, print the result
  if (carry != 0 && VERBOSE) {
    fprintf(stderr, "add: final carry %u\n", carry);
  }

  // Return `result` by moving by index
  return result + result_i;
}

char *arithmatoy_sub(unsigned int base, const char *lhs, const char *rhs) {
  if (VERBOSE) {
    fprintf(stderr, "sub: entering function\n");
  }

  // Fill the function, the goal is to compute lhs - rhs (assuming lhs > rhs)
  // You should allocate a new char* large enough to store the result as a
  // string Implement the algorithm Return the result
  
  // Verify if lhs == rhs, if so, the result will be 0
  lhs = drop_leading_zeros(lhs);
  rhs = drop_leading_zeros(rhs);

  if (strcmp(lhs, rhs) == 0){
    char *result = malloc(2);
    result[0] = '0';
    result[1] = '\0';
    return result;
  }

  //verify if rhs equal to 0. if so, the result will be equal to lhs
  else if (strcmp(rhs, "0") == 0){
    return strdup(lhs);
  }

  //verify if lhs > rhs, if not, retrun NULL
  size_t length_lhs = strlen(lhs), length_rhs = strlen(rhs);
  if (length_lhs < length_rhs || (length_lhs == length_rhs && strcmp(lhs, rhs)<0)){
    return NULL; 
  }

  //initiating variables for subbstraction op.
  size_t maximum_length = (length_lhs > length_rhs) ? length_lhs : length_rhs;
  int carry = 0;
  int result_i = maximum_length -1;
  //Memo allocation to store the result, 1byte for NULL, 1byte for the carry
  char *result = malloc(maximum_length+2);
  result[result_i +1] = '\0'; 

  // going through `lhs` and `rhs` from the right side to the left side, addinig digit by digit and saving the result -> `result`
  while (length_lhs > 0 || length_rhs > 0) {
    // getting the digits values to add
    size_t digit_lhs = (length_lhs > 0 ? get_digit_value(lhs[--length_lhs]) : 0);
    size_t digit_rhs = (length_rhs > 0 ? get_digit_value(rhs[--length_rhs]) : 0);
    if (VERBOSE){
      fprintf(stderr, "Substraction: Digit %c Digit %c carry %u\n", to_digit(digit_lhs), to_digit(digit_rhs), carry); 
    }

    int diff = digit_lhs - digit_rhs - carry + base;
    carry = (diff >= base ? 0 : 1);
    size_t digit = diff % base;
    if (VERBOSE){
      fprintf(stderr, "Substraction result: Digit %c Digit %c carry %u\n", to_digit(digit), carry);
    }
    result[result_i--] = to_digit(digit);
  }
  while(result[result_i+1] == '0') {
    ++result_i;
  }

  if (result_i == maximum_length -1){
    return NULL;
  }else{
    return result + result_i +1;
  }



}

char *arithmatoy_mul(unsigned int base, const char *lhs, const char *rhs) {
  if (VERBOSE) {
    fprintf(stderr, "mul: entering function\n");
  }

  // Fill the function, the goal is to compute lhs * rhs
  // You should allocate a new char* large enough to store the result as a
  // string Implement the algorithm Return the result
  // Verify if lhs == rhs, if so, the result will be 0
  lhs = drop_leading_zeros(lhs);
  rhs = drop_leading_zeros(rhs);

  if (strcmp(lhs, "0") ==0 || strcmp(rhs, "0") == 0){
    return "0";
  }

  size_t length_lhs = strlen(lhs), length_rhs = strlen(rhs);

  size_t maximum_length = length_lhs + length_rhs;

  unsigned int *result = calloc(maximum_length, sizeof(unsigned int));

  for(size_t i = 0; i < length_lhs; i++){
    unsigned int carry = 0;
    unsigned int digit_lhs = get_digit_value(lhs[length_lhs - 1 - i]);
    for(size_t j = 0; j< length_rhs; j++){
      unsigned int digit_rhs = get_digit_value(rhs[length_rhs - 1 - j]);
      unsigned int sum = result [i+j] + digit_lhs * digit_rhs + carry;
      result[i+j] = sum %base;
      carry = sum /base;
    }
    if(carry>0){
      result[i+length_rhs] += carry;
    }
  }

  size_t result_length = maximum_length;
  while (result_length > 1 && result[result_length-1] == 0){
    result_length--;
  }

  char *result_string = calloc(result_length+1, sizeof(char));
  for(size_t i = 0; i<result_length; i++){
    result_string[result_length-1-i] = to_digit(result[i]);
  }

  result_string[result_length] = '\0';

  return result_string;


}

// Here are some utility functions that might be helpful to implement add, sub
// and mul:

unsigned int get_digit_value(char digit) {
  // Convert a digit from get_all_digits() to its integer value
  if (digit >= '0' && digit <= '9') {
    return digit - '0';
  }
  if (digit >= 'a' && digit <= 'z') {
    return 10 + (digit - 'a');
  }
  return -1;
}

char to_digit(unsigned int value) {
  // Convert an integer value to a digit from get_all_digits()
  if (value >= ALL_DIGIT_COUNT) {
    debug_abort("Invalid value for to_digit()");
    return 0;
  }
  return get_all_digits()[value];
}

char *reverse(char *str) {
  // Reverse a string in place, return the pointer for convenience
  // Might be helpful if you fill your char* buffer from left to right
  const size_t length = strlen(str);
  const size_t bound = length / 2;
  for (size_t i = 0; i < bound; ++i) {
    char tmp = str[i];
    const size_t mirror = length - i - 1;
    str[i] = str[mirror];
    str[mirror] = tmp;
  }
  return str;
}

const char *drop_leading_zeros(const char *number) {
  // If the number has leading zeros, return a pointer past these zeros
  // Might be helpful to avoid computing a result with leading zeros
  if (*number == '\0') {
    return number;
  }
  while (*number == '0') {
    ++number;
  }
  if (*number == '\0') {
    --number;
  }
  return number;
}

void debug_abort(const char *debug_msg) {
  // Print a message and exit
  fprintf(stderr, debug_msg);
  exit(EXIT_FAILURE);
}
